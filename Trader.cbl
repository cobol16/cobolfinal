       IDENTIFICATION DIVISION.
       PROGRAM-ID.TRADER.
       AUTHOR.NATRUJA.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT TRADER-REPORT-FILE ASSIGN TO "trader.rpt"
              ORGANIZATION IS  SEQUENTIAL.
           SELECT TRADER-FILE ASSIGN TO "trader2.dat"
              ORGANIZATION IS LINE  SEQUENTIAL.
       DATA DIVISION.
       FILE SECTION.
       FD  TRADER-REPORT-FILE.
       01  TRADERDETAIL.
           88 END-OF-TRADER-FILE VALUE HIGH-VALUES.
              05 Province    PIC 9(2).
              05 PINCOME     PIC $,999,999,999.
              05 Member      PIC 9(4).
              05 Memincome   PIC $,999,999,999.

       WORKING-STORAGE SECTION.
       01  PRN-TOTAL-PROVINCE.
           05 FILLER          PIC X(15) VALUE "MAX PROVINCE: ".
           05 TOTAL-PROVINCE  PIC 9(2).
       01  PRN-TOTAL-INCOME.
           05 FILLER          PIC X(15) VALUE "SUM INCOME : ".
           05  TOTAL-INCOME   PIC 9(10).
       
       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT TRADER-FILE .
          
           READ TRADER-FILE
              AT END SET END-OF-TRADER-FILE TO TRUE 
           END-READ
           PERFORM UNTIL END-OF-TRADER-FILE 
              READ  TRADER-FILE 
                 AT END SET END-OF-TRADER-FILE TO TRUE 
              END-READ
           END-PERFORM

           CLOSE TRADER-FILE
           STOP RUN.
